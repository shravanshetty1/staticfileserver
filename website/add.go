package website

import (
	"archive/zip"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
	"unicode"

	"gitlab.com/shravanshetty1/freeWebhosting/pkg"

	"gitlab.com/shravanshetty1/centurion"

	"gitlab.com/shravanshetty1/freeWebhosting/models"

	"github.com/dgraph-io/badger"
)

func Add(c *centurion.Context, w http.ResponseWriter, r *http.Request) {
	password := r.PostFormValue("password")
	err := checkPasswordStrength(password)
	if err != nil {
		c.Error(w).Client(err.Error())
		return
	}
	websiteNameToData, _ := c.InjectedDependencies.Get("websiteCache").(*pkg.WebsiteCache)

	uploadedFile, fileHeader, err := r.FormFile("website")
	if err != nil {
		c.Error(w).InvalidParam("website")
		return
	}

	if filepath.Ext(fileHeader.Filename) != "zip" {
		c.Error(w).InvalidParam("website")
	}

	zippedFile, err := zip.NewReader(uploadedFile, fileHeader.Size)
	if err != nil {
		c.Error(w).InvalidParam("website")
		return
	}

	websiteName := generateWebsiteName(c.Badger)
	err = unzipFile(zippedFile, filepath.Join(pkg.BaseDir, "websites", websiteName))
	if err != nil {
		c.Error(w).Generic("Could not unzip:" + err.Error())
		return
	}

	websiteInfo := &models.WebsiteInformation{
		Password: getHash(password, "asldflhadsfhlasdf@!#!@123123kkdsdkf"),
		Email:    r.PostFormValue("email"),
	}
	websiteNameToData.Set(websiteName, websiteInfo)

	jsonEncodedWebsiteData, _ := json.Marshal(websiteInfo)
	err = c.Badger.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(websiteName), jsonEncodedWebsiteData)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		c.Error(w).Generic("Could not set to db:" + err.Error())
		return
	}

	return
}

func generateWebsiteName(db *badger.DB) string {
	websiteName := strconv.Itoa(rand.New(rand.NewSource(time.Now().UnixNano())).Int()) + ".shravanshetty.net"
	err := db.View(func(txn *badger.Txn) error {
		_, err := txn.Get([]byte(websiteName))
		return err
	})
	if err == badger.ErrKeyNotFound {
		return websiteName
	}
	return generateWebsiteName(db)
}

func unzipFile(zippedFolder *zip.Reader, targetDir string) error {
	allowedFileExt := map[string]bool{
		".js":   true,
		".css":  true,
		".html": true,
	}

	for _, file := range zippedFolder.File {
		err := func() error {
			zippedFile, err := file.Open()
			defer zippedFile.Close()
			if err != nil {
				return err
			}

			extractedFilePath := filepath.Clean(filepath.Join(targetDir, file.Name) + string(os.PathSeparator))
			if file.FileInfo().IsDir() {
				err := os.MkdirAll(extractedFilePath, 0777)
				if err != nil {
					return err
				}
				return nil
			}

			if !allowedFileExt[filepath.Ext(file.Name)] {
				log.Println("invalid filepath - " + filepath.Ext(file.Name))
				return nil
			}

			outputFile, err := os.OpenFile(extractedFilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0777)
			if err != nil {
				return err
			}
			defer outputFile.Close()

			_, err = io.Copy(outputFile, zippedFile)
			if err != nil {
				return err
			}
			return nil
		}()
		if err != nil {
			return err
		}
	}
	return nil
}

func checkPasswordStrength(password string) error {
	// Set defaults
	config := map[string]int{
		"minPasswordLength":             8,
		"minNumberOfNumericCharacters":  1,
		"minNumberOfUppercaseAlphabets": 1,
		"minNumberOfLowercaseAlphabets": 0,
		"minNumberOfSymbols":            0,
	}

	if len(password) < config["minPasswordLength"] {
		return errors.New("Your password should be atleast of length " + strconv.Itoa(config["minPasswordLength"]))
	}

	passwordProperties := map[string]int{}
	for _, v := range password {
		switch {
		case unicode.IsLower(v):
			passwordProperties["lowercase"]++
		case unicode.IsNumber(v):
			passwordProperties["numbers"]++
		case unicode.IsUpper(v):
			passwordProperties["uppercase"]++
		case unicode.IsSymbol(v):
			passwordProperties["symbols"]++
		}
	}

	switch {
	case passwordProperties["numbers"] < config["minNumberOfNumericCharacters"]:
		return errors.New("Your password should have atleast " + strconv.Itoa(config["minNumberOfNumericCharacters"]) + " numeric character(s).")
	case passwordProperties["uppercase"] < config["minNumberOfUppercaseAlphabets"]:
		return errors.New("Your password should have atleast " + strconv.Itoa(config["minNumberOfUppercaseAlphabets"]) + " uppercase alphabet(s).")
	case passwordProperties["lowercase"] < config["minNumberOfLowercaseAlphabets"]:
		return errors.New("Your password should have atleast " + strconv.Itoa(config["minNumberOfLowercaseAlphabets"]) + " lowercase alphabet(s).")
	case passwordProperties["symbols"] < config["minNumberOfSymbols"]:
		return errors.New("Your password should have atleast " + strconv.Itoa(config["minNumberOfSymbols"]) + " symbol(s).")
	}

	return nil
}

func getHash(password string, salt string) string {
	hash := sha256.New()
	hash.Write([]byte(password))
	if salt != "" {
		hash.Write([]byte(salt))
	}
	return fmt.Sprintf("%x", hash.Sum(nil))
}
