package website

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"gitlab.com/shravanshetty1/freeWebhosting/models"

	"github.com/dgraph-io/badger"

	"gitlab.com/shravanshetty1/centurion"
)

func Login(c *centurion.Context, w http.ResponseWriter, r *http.Request) {
	params := map[string]string{}
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		c.Error(w).Generic("invalid body")
		return
	}

	websiteName := params["websiteName"]
	password := params["password"]

	if websiteName == "" {
		c.Error(w).MissingParam("websiteName")
	}
	if password == "" {
		c.Error(w).MissingParam("password")
	}

	err = c.Badger.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(websiteName))
		if err != nil {
			return err
		}

		webInfo := models.WebsiteInformation{}
		val, err := item.ValueCopy(nil)
		if err != nil {
			return err
		}
		err = json.Unmarshal(val, &webInfo)
		if err != nil {
			return err
		}

		if webInfo.Password == getHash(password, "asldflhadsfhlasdf@!#!@123123kkdsdkf") {
			return nil
		}
		return errors.New("invalid password")
	})
	if err != nil {
		c.Error(w).Generic(err.Error())
	}

	c.Response(w).JSON(createAccessToken(websiteName))
	return
}

func createAccessToken(websiteName string) string {
	var temp struct {
		WebsiteName string `json:"websiteName"`
		TimeStamp   string `json:"timeStamp"`
	}
	temp.WebsiteName = websiteName
	temp.TimeStamp = time.Now().String()

	b, _ := json.Marshal(temp)

	return base64.StdEncoding.EncodeToString(b) + "." + getHash(string(b), "jsonsecretlulwjsonsecretlulwjsonsecretlulwjsonsecretlulw")
}
