// javascript sucks
typeWriter(0);
function typeWriter(buf) {
  var list = ['Backend Developer','Frontend Developer','Freelance Fullstack Developer'];
  var speed = 90;
  var element = document.getElementById("typewriter");
  for (let i = 0; i < list.length; i++) {
    let txt = list[i];
    for (let j = 0; j < txt.length; j++) {
      let addTime = (j*speed)+buf;
      let removeTime = ((j+txt.length)*speed)+500+buf;
      setTimeout(() => {
        element.innerHTML += txt.charAt(j);
      }, addTime);
      if (i==(list.length-1)){
        removeTime+=8000;
      }
      setTimeout(() => {
        element.innerHTML = element.innerHTML.slice(0,-1);
      }, removeTime);
    }
    buf+=(txt.length*2*speed)+500+1000;
  }
  if (buf<1000000){
    buf+=8000;
    typeWriter(buf)
  }
}

(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sideNav'
  });

})(jQuery); // End of use strict